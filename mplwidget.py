from PyQt5 import QtWidgets
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtCore import QSize
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
import matplotlib
import numpy as np
from matplotlib import pyplot as plt

# Ensure using PyQt5 backend
matplotlib.use('QT5Agg')


class MplCanvas(Canvas):
    """
    Matplotlib canvas class. Want to create the figure to the appropriate specifications for the window here
    The axis object belonging to the Figure will be created dynamically
    """
    def __init__(self, width, height):
        self.Fig = Figure(figsize=(width, height))
        self.Fig.set_tight_layout('tight')
        Canvas.__init__(self, self.Fig)
        Canvas.setSizePolicy(self, QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        Canvas.updateGeometry(self)


class MplWidget(QtWidgets.QWidget):
    """
    Matplotlib widget
    """
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)   # Inherit from QWidget
        self._vbl = QtWidgets.QVBoxLayout()         # Set box for plotting
        self._vbl.setContentsMargins(0, 0, 0, 0)
        self.Canvas = MplCanvas(self.width(), self.height())                  # Create canvas object
        self._vbl.addWidget(self.Canvas)
        self.setLayout(self._vbl)

    def resizeEvent(self, resizeEvent):
        oldSize = self.size()
        self.setFixedWidth(int(oldSize.height() * 1.2))
