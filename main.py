from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QFileDialog

import matplotlib.pyplot as plt
import datetime
import sys
import os
import pickle
import numpy as np
import pandas as pd
from functools import partial

from window import Ui_MainWindow
from classes.BgWorker import WorkerClass
from classes.SinglePass import SinglePass
from classes.HyperPass import HyperPass
from classes.ValidationSplit import ValidationExtHoldOut, ValidationHoldOut, ValidationCross
from classes.Export import Export


def exception_hook(exctype, value, traceback):
    _excepthook(exctype, value, traceback)
    sys.exit(1)


_excepthook = sys.excepthook
sys.excepthook = exception_hook


class Main(QtWidgets.QMainWindow):

    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Get the current date and time
        def time():
            self._time += 0.1
        self._timer = QTimer()
        self._timer.timeout.connect(time)
        self._timer.start(100)
        self._time = float(0)

        # Setting up the background thread and background worker
        self.thread = QThread()
        self.worker = None

        # Directories for importing/ exporting
        self._featDir = None
        self._baseDir = None
        self._supDir = dict()
        self._transDir = dict()
        self._fileType = None
        self._importType = ''
        self._export = None
        self._exportData = False

        # Selected Groupbox for Mutually Exclusive Groupboxes
        self._frSelectedGroupBox = self.ui.groupBoxDimRed
        self._estSelectedGroupBox = self.ui.groupBoxEstimator

        # Windows used to display data to user
        self._displayFig1 = None
        self._displayFig2 = None
        self._displayInfo = None

        # Main Data Containers
        self._data = None
        self._classData = {'train': None, 'test': None}

        # Collections of parameters for the classify class object
        self._classifierList = list()
        self._featNumList = list()
        self._featRedList = list()

        # List of actions
        self._featNormActions = list()
        self._valTypeActions = list()
        self._classMethodActions = list()
        self._splitTypeActions = list()
        self._intTestSetActions = list()
        self._valIterActions = list()
        self._valFoldsActions = list()

        # Initial Operations
        self.set_up_signals()
        self.ui.statusbar.showMessage('Please select the directory containing the feature file...')

    def set_up_signals(self):
        """
        Method for setting up the widget signals when form loads
        """

        def manage_mutex_groupbox(groupBox, groupBoxList, groupBoxType, status):
            """
            Method for achieving mutually exclusive checked group-boxes for user selected automated feature selection
            Important to note that it is possible to have no feature reduction gb selected but NOT no est gb selected
            :param groupBox: GroupBox [The group-box which has been toggled (and therefore excluded for operations)]
            :param groupBoxList: List{Groupbox} [List of all groupboxes which are mutually exclusive]
            :param groupBoxType: string
            :param status: Bool [Clicked event bool (triggered when one of the groupboxes is manually checked on/off)]
            """

            if groupBoxType == 'feat_red':
                self._frSelectedGroupBox = None
            elif groupBoxType == 'est':
                groupBox.setChecked(True)
                self._estSelectedGroupBox = groupBox

            if status:
                for groupBoxNon in [subGroupBox for subGroupBox in groupBoxList if subGroupBox != groupBox]:
                    groupBoxNon.setChecked(False)
                if groupBoxType == 'feat_red':
                    self._frSelectedGroupBox = groupBox

        def manage_validation_type(actionType, status):
            """
            Method for managing other objects when validation type changed
            :param actionType: string [validation type chosen]
            :param status: Bool
            """

            if not status:
                return
            if actionType == 'hold_out' or actionType == 'hold_out_ext':
                self.ui.actionConfusionFold.setChecked(False)
                self.ui.actionConfusionFold.setDisabled(True)
                self.ui.actionConfusionIteration.setChecked(False)
                self.ui.actionConfusionIteration.setDisabled(True)
            elif actionType == 'cross_val':
                self.ui.actionConfusionFold.setEnabled(True)
                self.ui.actionConfusionIteration.setEnabled(True)

        # Import
        self.ui.pushButtonImportDir.clicked.connect(partial(self.get_dir, 'Feature', None))
        self.ui.pushButtonImport.clicked.connect(self.get_import)

        # Process
        self.ui.pushButtonImportSupLabel.clicked.connect(partial(self.get_dir, 'Supplementary', 'label'))
        self.ui.pushButtonImportSupTrial.clicked.connect(partial(self.get_dir, 'Supplementary', 'trial'))
        self.ui.pushButtonTransSupLabel.clicked.connect(partial(self.get_dir, 'Transform', 'label'))
        self.ui.pushButtonTransSupTrial.clicked.connect(partial(self.get_dir, 'Transform', 'trial'))
        self.ui.pushButtonProcess.clicked.connect(self.get_process)

        # Classification Single-pass
        featRedGbList = [self.ui.groupBoxDimRed, self.ui.groupBoxUniRed, self.ui.groupBoxRecRed, self.ui.groupBoxImpRed]
        for gb in featRedGbList:
            gb.clicked.connect(partial(manage_mutex_groupbox, gb, featRedGbList, 'feat_red'))
        estGbList = [self.ui.groupBoxEstimator, self.ui.groupBoxEstimatorNn]
        for gb in estGbList:
            gb.clicked.connect(partial(manage_mutex_groupbox, gb, estGbList, 'est'))
        self.ui.pushButtonClassify.clicked.connect(self.get_single_pass)

        # Classification Hyper-parameters
        self.ui.pushButtonAddFeatNum.clicked.connect(partial(self.get_add_list_widget, 'FeatNum'))
        self.ui.pushButtonClearFeatNum.clicked.connect(partial(self.get_clear_list_widget, 'FeatNum'))
        self.ui.pushButtonAddFeatRed.clicked.connect(partial(self.get_add_list_widget, 'FeatRed'))
        self.ui.pushButtonClearFeatRed.clicked.connect(partial(self.get_clear_list_widget, 'FeatRed'))
        self.ui.pushButtonAddClassifierCh.clicked.connect(partial(self.get_add_list_widget, 'ClassifierCh'))
        self.ui.pushButtonClearClassifiersCh.clicked.connect(partial(self.get_clear_list_widget, 'ClassifierCh'))
        self.ui.pushButtonHyperClassify.clicked.connect(self.get_hyper_pass)

        # Menu Actions
        self.ui.menuCrossVal.menuAction().setDisabled(True)
        self.ui.actionValHoldOut.toggled.connect(lambda x: self.ui.menuHoldOut.menuAction().setEnabled(x))
        self.ui.actionValCrossVal.toggled.connect(lambda x: self.ui.menuCrossVal.menuAction().setEnabled(x))

        classMethodActionGroup = QtWidgets.QActionGroup(self)
        self._classMethodActions = [self.ui.actionClassMethodNone, self.ui.actionClassMethodOver,
                                    self.ui.actionClassMethodUnder, self.ui.actionClassMethodSynthetic]
        for action in self._classMethodActions:
            classMethodActionGroup.addAction(action)

        splitActionsGroup = QtWidgets.QActionGroup(self)
        self._splitTypeActions = [self.ui.actionSplitRandom, self.ui.actionSplitTrial]
        for action in self._splitTypeActions:
            splitActionsGroup.addAction(action)

        featNormActionGroup = QtWidgets.QActionGroup(self)
        self._featNormActions = [self.ui.actionNormNone, self.ui.actionNormStandardisation, self.ui.actionNormMean,
                                 self.ui.actionNormMaxMin]
        for action in self._featNormActions:
            featNormActionGroup.addAction(action)

        valMethodActionGroup = QtWidgets.QActionGroup(self)
        self._valTypeActions = [self.ui.actionValHoldOut, self.ui.actionValExtHoldOut, self.ui.actionValCrossVal]
        valType = ['hold_out', 'hold_out_ext', 'cross_val']
        for action, actionType in zip(self._valTypeActions, valType):
            valMethodActionGroup.addAction(action)
            action.toggled.connect(partial(manage_validation_type, actionType))

        intTestSetActionGroup = QtWidgets.QActionGroup(self)
        self._intTestSetActions = [self.ui.actionTest10, self.ui.actionTest20, self.ui.actionTest30,
                                   self.ui.actionTest40, self.ui.actionTest50]
        for action in self._intTestSetActions:
            intTestSetActionGroup.addAction(action)

        valIterActionGroup = QtWidgets.QActionGroup(self)
        self._valIterActions = [self.ui.actionIter1, self.ui.actionIter5, self.ui.actionIter10]
        for action in self._valIterActions:
            valIterActionGroup.addAction(action)

        valFoldsActionGroup = QtWidgets.QActionGroup(self)
        self._valFoldsActions = [self.ui.actionFold2, self.ui.actionFold5, self.ui.actionFold10]
        for action in self._valFoldsActions:
            valFoldsActionGroup.addAction(action)

    # IMPORT
    @pyqtSlot()
    def get_dir(self, baseType, subType):
        """
        Button triggered method for importing selecting the directory of the label, features, and text files
        """

        dlg = QFileDialog()
        if baseType == 'Feature':
            dlg.setFileMode(QFileDialog.ExistingFile)
            dlg.setNameFilter("Pickle or Parquet File (*.pickle *.parquet.gzip)")
        elif baseType == 'Supplementary' or baseType == 'Transform':
            dlg.setFileMode(QFileDialog.ExistingFile)
            dlg.setNameFilter("CSV File (*.csv)")

        if dlg.exec_():
            if baseType == 'Feature':
                self._featDir = dlg.selectedFiles()[0]
                self._baseDir = os.path.dirname(self._featDir)
                self._fileType = os.path.splitext(self._featDir)[1]
                self.ui.lineEditImportDir.setText(self._featDir)
                self.ui.pushButtonImport.setEnabled(True)
            elif baseType == 'Supplementary':
                self._supDir[subType] = dlg.selectedFiles()[0]
                if subType == 'label':
                    self.ui.lineEditImportSupLabel.setText(self._supDir[subType])
                elif subType == 'trial':
                    self.ui.lineEditImportSupTrial.setText(self._supDir[subType])
            elif baseType == 'Transform':
                self._transDir[subType] = dlg.selectedFiles()[0]
                if subType == 'label':
                    self.ui.lineEditTransSupLabel.setText(self._transDir[subType])
                elif subType == 'subject':
                    self.ui.lineEditTransSupTrial.setText(self._transDir[subType])
            self.ui.statusbar.showMessage(baseType + ' Directory Selected')
        else:
            self.ui.statusbar.showMessage('Directory Closed')

    @pyqtSlot()
    def get_import(self):
        """
        Button triggered method for importing the label and feature data files
        Most of processing performed on the background thread using the "importWorker" method
        """

        self._importType = self.ui.comboBoxDataType.currentText().lower()

        self.worker = WorkerClass(self.get_import_thread, self._featDir, self._fileType)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMessage.connect(self.update_message)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run_function)
        self.thread.start()

        self.update_message('import', 'Importing Data', 'update')
        self.ui.pushButtonImport.setDisabled(True)

    def get_import_thread(self, featDir, dataType, updateProg, updateMessage, updateData=None):
        """
        Worker for importing the pickle feature DataFrame
        :param featDir: string [directory of the datafile]
        :param dataType: string [compression type of the datafile]
        :param updateProg: Signal [Signal used to update the progressbar representing importing progress]
        :param updateMessage: Signal [Signal used to provide messages to the user]
        :param updateData: Signal [not used at this stage]
        :return importMessage: String [Returns message to be displayed to user on import completion]
        """

        def calc_metrics(data):
            """
            Method for calculating the number of non features in the columns and which indices were recorded
            :param data: pd.DataFrame
            """

            numNonFeatures = 0
            idxRec = {'label': False, 'trial': False}
            for metric in idxRec:
                if metric in list(data.columns.values):
                    numNonFeatures += 1
                    idxRec[metric] = True
            numFeatures = data.shape[1] - numNonFeatures
            return idxRec, numFeatures

        def update_import_desc(data, idxRec, numFeatures):
            """
            Method for updating the user to the import process
            :param data: pd.DataFrame
            :param idxRec:
            :param numFeatures:
            """

            self.update_message('import', 'Total Number of Features: %d' % numFeatures, 'info')
            self.update_message('import', 'Total Number of Samples: %d' % data.shape[0], 'info')
            if idxRec['label']:
                self.update_message('process', '"label" column present', 'info')
                uniqueLabels, numLabels = np.unique(data['label'], return_counts=True)
                self.update_message('import', 'Number of Unique Classes to Classify: %d [%s]'
                                    % (len(uniqueLabels), ', '.join(str(label) for label in uniqueLabels)), 'info')
                self.update_message('import', 'Class Distribution: Total: %d, %s'
                                    % (np.sum(numLabels), str(dict(zip(uniqueLabels, numLabels)))), 'info')
            else:
                self.update_message('process', '"label" column not found in data (must be added manually)', 'info')
            if idxRec['trial']:
                self.update_message('process', '"trial" column present', 'info')
                uniquePts = self._data['trial'].unique()
                self.update_message('import', 'Number of Trials Detected: %d [%s]'
                                    % (len(uniquePts), ', '.join(str(pt) for pt in uniquePts)), 'info')
            else:
                self.update_message('process', '"trial" column not found in data (may be added manually)', 'info')

        # Importing datafile
        if dataType == '.pickle':
            data = pd.read_pickle(featDir)
        elif dataType == '.parquet.gzip':
            data = pd.read_parquet(featDir)

        # Converting and checking data format
        if isinstance(data, np.ndarray):
            data = pd.DataFrame(data)
        if not isinstance(data, pd.DataFrame):
            raise Exception('Imported data is a non-recognised data type (not pd.DataFrame or np.array)')

        # Concatenating multiple levels of columns if present since only need one level for analysis
        # Resetting multiple levels of indices in place if present to ensure data in consistent state
        if data.columns.nlevels > 1:
            data.columns = ['_'.join(map(str, col)) for col in data.columns.values]
        if (data.index.names[0] is not None) or (data.index.nlevels > 1):
            existCols = list(data.columns.values)
            data.reset_index(inplace=True)
            idxCols = list(set(list(data.columns.values)) - set(existCols))
            for col in idxCols:
                if col not in ['label', 'trial']:
                    data.drop(col, axis=1, inplace=True)
        self._data = data

        # Displaying messages to the user
        idxRec, numFeatures = calc_metrics(self._data)
        update_import_desc(data, idxRec, numFeatures)

        # Update messages
        self.ui.pushButtonProcess.setEnabled(True)
        updateMessage.emit('import', 'Data Type Imported: %s' % self._importType.upper(), 'update')
        updateProg.emit('import', 1.0)
        updateMessage.emit('import', 'Completed Importing Data', 'update')
        return 'import'

    # PROCESS
    @pyqtSlot()
    def get_process(self):
        """
        Button triggered method for processing data and importing optional and necessary indices
        Processing performed on the background thread using the "process_data" method
        """

        self.worker = WorkerClass(self.get_process_thread, self._supDir, self._transDir)
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMessage.connect(self.update_message)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run_function)
        self.thread.start()

        self.update_message('process', 'Processing Data', 'update')
        self.ui.pushButtonProcess.setDisabled(True)

    def get_process_thread(self, supDir, transDir, updateProg, updateMessage, updateData=None):
        """
        Worker for importing and applying methods for applying and transforming supplementary data
        :param supDir: dict{String} [Collection of import directories]
        :param transDir: dict{String} [Collection of transformation directories]
        :param updateProg: Signal [Signal used to provide messages to the user]
        :param updateMessage: Signal [Signal used to provide messages to the user]
        :return processMessage: String [Returns message to be displayed to user on import completion]
        """

        def check_consistency(trainData, testData):
            """
            Method for checking that the training and test data imported share the same feature columns
            :param trainData: pd.DataFrame
            :param testData: pd.DataFrame
            """

            if list(trainData.columns.values) == list(testData.columns.values):
                self.update_message('process', 'Identical feature columns found between train and test set', 'update')
            elif len(trainData.columns.values) == len(testData.columns.values):
                self.update_message('process', 'Non-Identical feature columns found between train and test set', 'update')
            else:
                raise Exception('Different number columns between train and test set. Cannot perform classification')

        # Providing the supplementary columns
        for supCol in supDir:
            if supDir[supCol] != '':
                self._data[supCol] = pd.read_csv(supDir[supCol])

        # Transforming the supplementary columns
        for supCol in transDir:
            if transDir[supCol] == '':
                pass
            if supCol.lower() not in [col.lower() for col in self._data.columns.values]:
                updateMessage.emit(
                    'process', '%s column could not be transformed since it was not found in the data' % supCol, 'update')
                raise Exception('noncritical')
            transData = pd.read_csv(transDir[supCol])
            transDict = dict(zip(transData['original'], transData['transform']))
            self._data[supCol] = self._data[supCol].replace(transDict)

        # Verifying existence of supplmentary columns
        if 'label' not in [col.lower() for col in self._data.columns.values]:
            updateMessage.emit('process', '"label" column not found in data (please try again)', 'update')
            raise Exception('noncritical')
        if 'trial' not in [col.lower() for col in self._data.columns.values]:
            updateMessage.emit(
                'process', '"trial" column not found in data (not possible to split data by trial)', 'update')
            self._data.set_index(['label'], inplace=True)
        else:
            self._data.set_index(['label', 'trial'], inplace=True)

        # Check for nan in the data (first checks for features which are all nan then trials containing nan)
        if self._data.isna().all(axis=0).any():
            featNanIdx = [idx for idx, x in enumerate(self._data.isna().all(axis=0)) if x]
            updateMessage.emit('process', 'Nan features columns removed: %s [%s]'
                               % (len(featNanIdx), ', '.join(str(idx) for idx in featNanIdx)), 'info')
            self._data = self._data[self._data.columns[~self._data.isna().all(axis=0)]]
        if self._data.isna().any(axis=1).any():
            trialNanIdx = [idx for idx, x in enumerate(self._data.isna().any(axis=1)) if x]
            updateMessage.emit('process', 'Nan trial rows removed: %s [%s]'
                               % (len(trialNanIdx),
                                  ', '.join(str(trial) for trial in self._data.iloc[trialNanIdx].index.get_level_values('trial'))), 'info')
            self._data = self._data.loc[self._data.index[~self._data.isna().any(axis=1)], :]

        # Viable features considered to be 0.5 * (min of number samples, features)
        numViableFeatures = int(min(self._data.shape[0], self._data.shape[1]) / 2)
        if numViableFeatures > 100:
            increment = int(numViableFeatures / 100) * 10
        else:
            increment = 5
        featRange = np.arange(increment, numViableFeatures, increment)
        self.ui.comboBoxFeatRedInt.addItems([str(feat) for feat in featRange])
        self.ui.comboBoxDimRedThresh.addItems([str(feat) for feat in featRange])
        self.ui.comboBoxUniRedThresh.addItems([str(feat) for feat in featRange])
        self.ui.comboBoxRecRedThresh.addItems([str(feat) for feat in featRange])

        # Assigning the new data
        self._classData[self._importType] = self._data
        del self._data

        # Method to ensure train and test data are compatible
        if (self._classData['train'] is not None) and (self._classData['test'] is not None):
            check_consistency(self._classData['train'], self._classData['test'])

        updateProg.emit('process', 1)
        updateMessage.emit('process', 'Completed Processing Data', 'update')
        return 'process'

    # SINGLE-PASS CLASSIFICATION
    @pyqtSlot()
    def get_single_pass(self):
        """
        Button triggered method for applying single-pass to the imported data
        """

        # Displaying information to the user
        self._displayInfo = self.ui.listWidgetClassifyText
        self._displayFig1 = self.ui.widgetClassifyFig1
        self._displayFig2 = self.ui.widgetClassifyFig2
        self._displayInfo.clear()
        self.ui.progressBarClassify.setValue(0)

        # Data Pre-processing and shuffle settings
        splitType = [splitAct.text() for splitAct in self._splitTypeActions if splitAct.isChecked()][0]
        resamp = [methodAction.text() for methodAction in self._classMethodActions if methodAction.isChecked()][0]
        featNorm = [normAction.text() for normAction in self._featNormActions if normAction.isChecked()][0]
        dataSettings = {'splitType': splitType, 'resamp': resamp, 'featNorm': featNorm}

        # Feature Reduction Method
        featRedType = 'No_Reduction'
        featRedSubType = 'None'
        featRedBound = 'None'
        if self._frSelectedGroupBox is not None:
            featRedType = self._frSelectedGroupBox.title()
            featRedComboBoxes = self._frSelectedGroupBox.findChildren(QtWidgets.QComboBox)
            for comboBox in featRedComboBoxes:
                if comboBox.property('Type') == 'Method':
                    featRedSubType = comboBox.currentText()
                elif comboBox.property('Type') == 'Components':
                    featRedBound = int(comboBox.currentText())
                elif comboBox.property('Type') == 'Threshold':
                    featRedBound = comboBox.currentText()
        featRedSettings = {'featRedType': featRedType, 'featRedSubType': featRedSubType, 'featRedBound': featRedBound}

        # Estimator Settings
        estType = self._estSelectedGroupBox.title()
        estName = self._estSelectedGroupBox.findChildren(QtWidgets.QComboBox)[0].currentText()
        estSettings = {'estType': estType, 'estName': estName}

        # Validation Settings
        if self.ui.actionValExtHoldOut.isChecked():
            numIter = 1
            valClass = ValidationExtHoldOut(extTestSet=self._classData['test'])
        elif self.ui.actionValHoldOut.isChecked():
            numIter = 1
            intTestSize = [intTest.text() for intTest in self._intTestSetActions if intTest.isChecked()][0]
            valClass = ValidationHoldOut(internalTestSize=int(intTestSize))
        elif self.ui.actionValCrossVal.isChecked():
            numIter = int([valIter.text() for valIter in self._valIterActions if valIter.isChecked()][0])
            foldText = [fold.text() for fold in self._valFoldsActions if fold.isChecked()][0]
            if foldText == 'Trials-1':
                numFolds = len(self._classData['train'].index.unique(level='trial'))
            else:
                numFolds = int(foldText)
            stratifyFolds = self.ui.actionCrossStratify.isChecked()
            valClass = ValidationCross(stratify=stratifyFolds, numFolds=numFolds)

        # Return settings (what information we want to be returned to the user)
        if self.ui.actionReturnExport.isChecked():
            self._exportData = True
            expDir = os.path.join(self._baseDir, valClass.Name + '_' + datetime.datetime.now().strftime("%Y%m%d_%H%M"))
            self._export = Export(expDir)
        confusionSettings = {'confusionPerFold': self.ui.actionConfusionFold.isChecked(),
                             'confusionPerIter': self.ui.actionConfusionIteration.isChecked(),
                             'confusionPerAnalysis': self.ui.actionConfusionAnalysis.isChecked()}

        # Creating a new worker class to run the "classifyWorker" method and setup its signals
        classificationModel = SinglePass(dataSettings, featRedSettings, estSettings, valClass, confusionSettings, numIter)
        self.worker = WorkerClass(classificationModel.get_classify_thread, self._classData['train'])
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMessage.connect(self.update_message)
        self.worker.updateData.connect(self.update_data)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run_function)
        self.thread.start()
        self.update_message('classify', 'Started Classification', 'statusbar')

    # HYPER-PARAMETER CLASSIFICATION
    @pyqtSlot()
    def get_add_list_widget(self, listType):
        """
        Button triggered method for adding classifiers to a widget list and dict
        :param listType
        """

        if listType == 'ClassifierCh':
            classifierText = self.ui.comboBoxClassifierH.currentText()
            if classifierText not in self._classifierList:
                self._classifierList.append(classifierText)
                self.ui.listWidgetClassifierCh.addItem(classifierText)
                self.ui.statusbar.showMessage('Classifier: %s added' % classifierText)
                if self.ui.comboBoxClassifierH.currentIndex() < self.ui.comboBoxClassifierH.count() - 1:
                    self.ui.comboBoxClassifierH.setCurrentIndex(self.ui.comboBoxClassifierH.currentIndex() + 1)
            else:
                self.ui.statusbar.showMessage('Error: duplicate classifier selected')
        elif listType == 'FeatNum':
            featNum = self.ui.comboBoxFeatRedInt.currentText()
            if featNum not in self._featNumList:
                self._featNumList.append(featNum)
                self.ui.listWidgetFeatNum.addItem(featNum)
                self.ui.statusbar.showMessage('Number of Features: %s added' % featNum)
                if self.ui.comboBoxFeatRedInt.currentIndex() < self.ui.comboBoxFeatRedInt.count() - 1:
                    self.ui.comboBoxFeatRedInt.setCurrentIndex(self.ui.comboBoxFeatRedInt.currentIndex() + 1)
            else:
                self.ui.statusbar.showMessage('Error: duplicate feature number selected')
        elif listType == 'FeatRed':
            featRedText = self.ui.comboBoxFeatRed.currentText()
            if featRedText not in self._featRedList:
                self._featRedList.append(featRedText)
                self.ui.listWidgetFeatRed.addItem(featRedText)
                self.ui.statusbar.showMessage('Feature Reduction Method: %s added' % featRedText)
                if self.ui.comboBoxFeatRed.currentIndex() < self.ui.comboBoxFeatRed.count() - 1:
                    self.ui.comboBoxFeatRed.setCurrentIndex(self.ui.comboBoxFeatRed.currentIndex() + 1)
            else:
                self.ui.statusbar.showMessage('Error: duplicate feature reduction algorithm selected')

    @pyqtSlot()
    def get_clear_list_widget(self, listType):
        """
        Button triggered method for clearing classifiers from the widget list and dict
        """

        if listType == 'ClassifierCh':
            self.ui.listWidgetClassifierCh.clear()
            self._classifierList = list()
        elif listType == 'FeatNum':
            self.ui.listWidgetFeatNum.clear()
        elif listType == 'FeatRed':
            self.ui.listWidgetFeatRed.clear()
            self._featRedDict = dict()

    @pyqtSlot()
    def get_hyper_pass(self):
        """
        Button triggered method for applying custom classification with hyper-parameter optimisation
        """

        # Displaying information to the user
        self._displayInfo = self.ui.listWidgetHyperClassify
        self._displayFig1 = self.ui.widgetHyperClassifyFig1
        self._displayFig2 = self.ui.widgetHyperClassifyFig2
        self._displayInfo.clear()
        self.ui.progressBarHyperClassify.setValue(0)

        # Check minimum user requirements
        if len(self._featNumList) == 0 or len(self._featRedList) == 0 or len(self._classifierList) == 0:
            self.update_message('classify', 'Select at least one for each of the classifier configurations', 'update')
            return

        # Data Pre-processing and shuffle settings
        splitType = [splitAct.text() for splitAct in self._splitTypeActions if splitAct.isChecked()][0]
        resamp = [methodAction.text() for methodAction in self._classMethodActions if methodAction.isChecked()][0]
        featNorm = [normAction.text() for normAction in self._featNormActions if normAction.isChecked()][0]
        dataSettings = {'splitType': splitType, 'resamp': resamp, 'featNorm': featNorm}

        # Feature Reduction Collections
        featRedSettings = {'featRedList': self._featRedList, 'featNumList': self._featNumList}

        # Estimator Settings
        estSettings = {'estNameList': self._classifierList, 'optimiseEst': self.ui.checkBoxOptimiseClass.isChecked()}

        # Validation Settings
        if self.ui.actionValExtHoldOut.isChecked():
            numIter = 1
            valClass = ValidationExtHoldOut(extTestSet=self._classData['test'])
        elif self.ui.actionValHoldOut.isChecked():
            numIter = 1
            intTestSize = [intTest.text() for intTest in self._intTestSetActions if intTest.isChecked()][0]
            valClass = ValidationHoldOut(internalTestSize=int(intTestSize))
        elif self.ui.actionValCrossVal.isChecked():
            numIter = int([valIter.text() for valIter in self._valIterActions if valIter.isChecked()][0])
            foldText = [fold.text() for fold in self._valFoldsActions if fold.isChecked()][0]
            if foldText == 'Trials-1':
                numFolds = len(self._classData['train'].index.unique(level='trial'))
            else:
                numFolds = int(foldText)
            stratifyFolds = self.ui.actionCrossStratify.isChecked()
            valClass = ValidationCross(stratify=stratifyFolds, numFolds=numFolds)

        # Return settings (what information we want to be returned to the user)
        if self.ui.actionReturnExport.isChecked():
            self._exportData = True
            expDir = os.path.join(self._baseDir,
                                  valClass.Name + '_' + datetime.datetime.now().strftime("%Y%m%d_%H%M"))
            self._export = Export(expDir)
        confusionSettings = {'confusionPerFold': self.ui.actionConfusionFold.isChecked(),
                             'confusionPerIter': self.ui.actionConfusionIteration.isChecked(),
                             'confusionPerAnalysis': self.ui.actionConfusionAnalysis.isChecked()}

        # Creating a new worker class to run the "classifyWorker" method and setup its signals
        classModel = HyperPass(dataSettings, featRedSettings, estSettings, valClass, confusionSettings, numIter)
        self.worker = WorkerClass(classModel.get_classify_thread, self._classData['train'])
        self.worker.updateProg.connect(self.update_progress)
        self.worker.updateMessage.connect(self.update_message)
        self.worker.updateData.connect(self.update_data)
        self.worker.finished.connect(self.thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run_function)
        self.thread.start()
        self.update_message('classify', 'Started Classification', 'statusbar')

    # RETURN
    def display_data(self, data, dataType, heading):
        """
        Method for displaying data results to the user
        :param data:
        :param dataType:
        :param heading
        :return:
        """

        if dataType == 'conf_matrix':
            for ax in self._displayFig1.Canvas.Fig.axes:
                ax.remove()
            data.generate_ax(self._displayFig1.Canvas.Fig, title=heading)
            self._displayFig1.Canvas.updateGeometry()
            self._displayFig1.Canvas.draw()
        elif dataType == 'conf_matrix_overall':
            for ax in self._displayFig2.Canvas.Fig.axes:
                ax.remove()
            data.generate_ax(self._displayFig2.Canvas.Fig, title=heading)
            self._displayFig2.Canvas.updateGeometry()
            self._displayFig2.Canvas.draw()
        else:
            self._displayInfo.addItem('**********' + heading + '**********')
            self._displayInfo.addItem(data)

    def export_data(self, data, dataType, heading):
        """
        Method dedicated to exporting classification data. Currently run on the main thread
        """

        if dataType == 'summary':
            self._export.export_text(data, 'summary')
        elif dataType == 'conf_matrix' or dataType == 'conf_matrix_overall':
            self._export.export_fig(data.as_fig(title=heading), heading)
        elif dataType == 'class_report':
            self._export.export_text(data, 'class_report')
        elif dataType == 'best_params' or dataType == 'all_params':
            self._export.export_text(data, dataType)

    # THREAD-SAFE OPERATIONS
    def update_progress(self, stage, progress):
        """
        Thread-safe method of updating progress bar for user
        :param stage: string [stage of processing]
        :param progress: float [value between 0 and 1]
        """

        if stage == 'import':
            self.ui.progressBarImport.setValue(int(progress * 100))
        elif stage == 'process':
            self.ui.progressBarProcess.setValue(int(progress * 100))
        elif stage == 'classify':
            self.ui.progressBarClassify.setValue(int(progress * 100))
        elif stage == 'hyperClassify':
            self.ui.progressBarHyperClassify.setValue(int(progress * 100))

    def update_message(self, stage, message, status):
        """
        Update Message method called by a background thread
        :param stage: string [the stage of the analysis]
        :param message: string [message to display to the user]
        :param status: string [message type]
        """

        if status == 'update':
            self.ui.statusbar.showMessage(message)
            message = '[STATUS] ' + message + ' (%.1f seconds)' % self._time
            print(message)
        elif status == 'info':
            message = '[%s] ' % self._importType.upper() + message
        elif status == 'statusbar':
            self.ui.statusbar.showMessage(message)

        if stage == 'import':
            self.ui.listWidgetImport.addItem(message)
        elif stage == 'process':
            self.ui.listWidgetProcess.addItem(message)

    def update_data(self, data, dataType, heading, display):
        """
        Thread safe method called during classification of data. Currently outputs only method to save results
        :param data: object [class object or string]
        :param dataType: string [description of data being sent]
        :param heading: string [to be used as message to user or heading of plot]
        :param display: bool [whether to print data to the user]
        """

        if display:
            self.display_data(data, dataType, heading)
        if self._exportData:
            self.export_data(data, dataType, heading)

    def thread_finished(self, stage):
        """
        Thread Finished method called by a background thread
        :param stage: string
        """

        if stage == 'import':
            self.ui.groupBoxSupp.setEnabled(True)
            self.ui.groupBoxTrans.setEnabled(True)
        elif stage == 'process':
            if 'trial' in self._classData[self._importType].index.names:
                self.ui.actionSplitTrial.setEnabled(True)
            else:
                self.ui.actionSplitTrial.setDisbled(True)
                self.ui.actionSplitTrial.setChecked(False)
            if self._importType == 'train':
                self.ui.pushButtonClassify.setEnabled(True)
                self.ui.pushButtonHyperClassify.setEnabled(True)
            elif self._importType == 'test':
                self.ui.actionValExtHoldOut.setEnabled(True)
        elif stage == 'classify' or stage == 'hyperClassify':
            pass
        elif stage == 'noncritical_except':
            pass

        self.thread.quit()
        self.worker = None


if __name__ == "__main__":
    # Defines a new application process
    app = QtWidgets.QApplication(sys.argv)

    # Create new UI MainWindow class object and assign the window widget to it
    mainWindow = Main()
    mainWindow.show()
    sys.exit(app.exec_())
