from sklearn.base import BaseEstimator


class BaseEstimate(BaseEstimator):

    def __init__(self, estimator):
        """
        A Custom BaseEstimator that can switch between classifiers.
        :param estimator: SKLearn Estimator
        """

        self.estimator = estimator

    def fit(self, X, y=None, **kwargs):
        self.estimator.fit(X, y)
        return self

    def predict(self, X, y=None):
        return self.estimator.predict(X)

    def predict_proba(self, X):
        return self.estimator.predict_proba(X)

    def score(self, X, y):
        return self.estimator.score(X, y)


class DummyEstimate(BaseEstimator):

    def __init__(self):
        pass

    def fit(self, X, y=None, **kwargs):
        return self

    def predict(self, X, y=None):
        return 1

    def score(self, X, y):
        return 0


class DummyFeatureNorm(BaseEstimator):

    def fit(self, data, y=None):
        return self

    def transform(self, data):
        return data


class DummyResample(BaseEstimator):

    def fit_resample(self, x, y):
        return x, y



