from classes.BaseEstimate import BaseEstimate, DummyEstimate, DummyFeatureNorm, DummyResample
from classes.BaseFeatSelect import BaseFeatSelect, DummyFeatSelect
from classes.ConfusionMatrix import ConfusionMatrix
from classes.neural_nets.cnn import cnn
from classes.neural_nets.rnn import rnn

import numpy as np
import pandas as pd
from collections import Counter

from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import ParameterGrid
from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import mutual_info_classif
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import RFE
from sklearn.ensemble import ExtraTreesClassifier


class HyperPass:

    def __init__(self, dataSettings, featRedSettings, estSettings, valClass, confusionSettings, numIter):
        """
        Abstraction class for classifying data using Pipeline(imblearn) for data transformation steps and
        GridSearchCV(sklearn) for the purposes of hyper-parameter optimisation
        :param dataSettings: Dict{String} [data pre-processing and split type]
        :param featRedSettings: Dict{List{string}} [collections of user selected feature reduction number/ methods]
        :param estSettings: Dict{List{string}/ Bool} [collection of estimator names and whether to optimise params]
        :param valClass: ValidationSplit Class Instance
        :param confusionSettings: Dict{bool}
        :param numIter: Int
        """

        # Data Settings
        self._splitType = dataSettings['splitType']
        resamp = {
            'None': DummyResample(),
            'Oversampling': RandomOverSampler(random_state=0),
            'Undersampling': RandomUnderSampler(random_state=0),
            'Synthetic Oversampling': SMOTE(random_state=0)
        }.get(dataSettings['resamp'])
        featNorm = {
            'None': DummyFeatureNorm(),
            'Standardisation': preprocessing.StandardScaler(),
            'Min-Max Normalisation': preprocessing.MinMaxScaler()
        }.get(dataSettings['featNorm'])

        # Feature Reduction Settings
        featRedList = self.get_feat_red(featRedSettings['featRedList'])
        numFeatList = list(map(int, featRedSettings['featNumList']))

        # Estimator Settings (new param grid generated per classifier tested)
        # Each param grid composed of single classifier, n perm for each classifier parameter,
        # n perm feature selection methods, n perm features preserved
        # Single classifier used since only way to iterate over classifier parameters
        estNameList = estSettings['estNameList']
        optimiseEst = estSettings['optimiseEst']
        paramGrid = list()
        for estText in estNameList:
            paramDict = self.get_class_param(estText, optimiseEst)
            paramDict['baseFeatRed__featSelect'] = featRedList
            paramDict['baseFeatRed__numFeat'] = numFeatList
            paramGrid.append(paramDict)

        # Validation Class
        self._valClass = valClass

        # Confusion Matrix Settings
        self._confusionSettings = confusionSettings

        # Pipeline set up
        pipeline = Pipeline([('featNorm', featNorm), ('resamp', resamp),
                             ('baseFeatRed', BaseFeatSelect(DummyFeatSelect())),
                             ('baseEstimator', BaseEstimate(DummyEstimate()))])
        self._gridSearch = GridSearchCV(pipeline, paramGrid, cv=5, verbose=1)
        self._gridIter = len(ParameterGrid(paramGrid)) * 5

        # Other parameters
        self._numIter = numIter

        # Summary
        dataSettings = ', '.join(key + ': ' + str(dataSettings[key]) for key in dataSettings)
        confSettings = ', '.join(key + ': ' + str(confusionSettings[key]) for key in confusionSettings)
        if valClass.Name == 'ext_holdout':
            valSettings = 'Name: ext_holdout'
        else:
            valSettings = ', '.join(key + ': ' + str(valClass.__dict__[key]) for key in valClass.__dict__)
        self._summary = 'NUM ITER: ' + str(numIter) + '\nVAL SETTINGS: ' + valSettings + \
                        '\nDATA SETTINGS: ' + dataSettings + \
                        '\nFEAT METHODS: ' + ', '.join(featRedSettings['featRedList']) + \
                        '\nFEAT NUM: ' + ', '.join(featRedSettings['featNumList']) + \
                        '\nESTIMATORS: ' + ', '.join(estNameList) + '\nOPT ESTIMATORS: ' + str(optimiseEst) + \
                        '\nCONFUSION SETTINGS: ' + confSettings

    @staticmethod
    def get_feat_red(featRedNameList):
        """
        Method for appending the appropriate feature reduction method given the corresponding string
        :param featRedNameList: List{String}
        :return featRedList: List{Method}
        """

        featRedList = list()
        for featRed in featRedNameList:
            if featRed == 'No Reduction':
                featRedList.append(DummyFeatSelect())
            elif featRed == 'PCA':
                featRedList.append(PCA())
            elif featRed == 'LDA':
                featRedList.append(LDA())
            elif featRed == 'Univariate':
                featRedList.append(SelectKBest(chi2))
            elif featRed == 'Recursive':
                featRedList.append(RFE(estimator=SVC(kernel="linear")))

        return featRedList

    @staticmethod
    def get_class_param(classText, optimiseParam):
        """
        Method for getting a list of classifier objects and optimisation parameters from corresponding strings
        :param classText: String [User selected classifier as string]
        :param optimiseParam: Bool [Whether or not the user wants to optimise hyper-parameters for selected classifier]
        :return paramDict: Dict{List{Int/string}} [Keys are classifier params]
        """

        classParam, classifier = None, None
        if classText == 'Logistic Regression':
            classifier = LogisticRegression(multi_class='auto', solver='lbfgs', tol=0.0001)
            classParam = {'solver': ['newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'], 'C': [0.01, 0.001, 0.0001]}
        elif classText == 'Linear Discriminant Analysis':
            classifier = LDA()
            classParam = {'solver': ['svd', 'lsqr', 'eigen']}
        elif classText == 'K Nearest Neighbours':
            classifier = KNN()
            classParam = {'n_neighbors': [3, 5, 10, 20], 'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'],
                          'leaf_size': [20, 30, 40, 50]}
        elif classText == 'Decision Tree':
            classifier = DecisionTreeClassifier()
        elif classText == 'Support Vector Classifier':
            classifier = SVC(gamma='auto')
            classParam = {'kernel': ('linear', 'rbf'), 'gamma': [0.001, 0.0001], 'C': [1, 10, 100, 1000]}
        elif classText == 'CNN':
            classifier = cnn(windowSize=200)
            classParam = {'filter1': (64, 128, 256), 'filter2': (32, 64, 128),
                          'lossFunction': ('categorical_crossentropy', 'mean_squared_error', 'categorical_hinge')}
        elif classText == 'RNN':
            classifier = rnn(windowSize=200)
            classParam = {'lossFunction': ('categorical_crossentropy', 'mean_squared_error', 'categorical_hinge')}

        # Setting the base estimator
        paramDict = dict()
        paramDict['baseEstimator__estimator'] = [classifier]
        # Setting the base estimator parameters
        if classParam is not None and optimiseParam:
            for key in classParam:
                paramDict['baseEstimator__estimator' + '__' + key] = classParam[key]
        return paramDict

    @staticmethod
    def get_best_param(bestParamList):

        def most_frequent(bestList):
            occurence_count = Counter(bestList)
            return occurence_count.most_common(1)[0][0]

        bestEstimators = list()
        bestFeatRed = list()
        bestFeatNum = list()
        for paramDict in bestParamList:
            bestEstimators.append(str(type(paramDict['baseEstimator__estimator'])))
            bestFeatRed.append(str(type(paramDict['baseFeatRed__featSelect'])))
            bestFeatNum.append(str(paramDict['baseFeatRed__numFeat']))
        bestList = 'BEST ESTIMATOR: ' + most_frequent(bestEstimators) + \
                   '\nBEST FEAT RED: ' + most_frequent(bestFeatRed) + \
                   '\nBEST FEAT NUM: ' + most_frequent(bestFeatNum)
        return bestList

    def get_classify_thread(self, data, updateProg, updateMessage, updateData):
        """
        Method for performing classification on a background thread
        :param data: pd.DataFrame
        :param updateProg: Signal
        :param updateMessage: Signal
        :param updateData: Signal
        :return: string
        """

        updateData.emit(self._summary, 'summary', 'summary', True)
        labelDfList = list()
        bestParamList = list()
        for iteration in range(self._numIter):
            labelDf, bestParam = self.class_data(data, self._gridSearch, self._valClass, iteration, self._splitType, updateData)
            labelDfList.append(labelDf)
            bestParamList = bestParamList + bestParam
            if self._confusionSettings['confusionPerIter']:
                confMat = ConfusionMatrix(labelDf['trueLabel'], labelDf['predLabel'], classes=np.unique(labelDf['trueLabel']))
                updateData.emit(confMat, 'conf_matrix', 'iter_%d_folds_all' % iteration, True)
            updateProg.emit('classify', iteration / float(self._numIter))

        labelDf = pd.concat(labelDfList, axis=0)
        updateData.emit(self.get_best_param(bestParamList), 'best_params', 'best_params', True)
        updateData.emit('\n'.join(str(paramDict) for paramDict in bestParamList), 'all_params', 'all_params', False)
        if self._confusionSettings['confusionPerAnalysis']:
            confMat = ConfusionMatrix(labelDf['trueLabel'], labelDf['predLabel'], classes=np.unique(labelDf['trueLabel']))
            updateData.emit(confMat, 'conf_matrix_overall', 'iter_all_folds_all', True)
        classReport = classification_report(labelDf['trueLabel'], labelDf['predLabel'])
        updateData.emit(classReport, 'class_report', 'class_report', True)

        updateProg.emit('hyperClassify', 1)
        updateMessage.emit('hyperClassify', 'Completed Classification', 'statusbar')
        return 'hyperClassify'

    def class_data(self, data, gridSearch, valClass, iteration, splitType, updateData):
        """
        Implementation of random shuffle validation method
        :param data: pd.DataFrame [Index is PT/Window] [Columns are Features]
        :param gridSearch
        :param valClass
        :param iteration: int
        :param splitType
        :param updateData
        :return labelDf
        """

        fold = 0
        iterTrials = np.array([])
        iterTestLabels = np.array([])
        iterPredLabels = np.array([])
        bestParamList = []
        trainData, testData, trainLabels, testLabels, trials = [], [], [], [], []
        if splitType == 'Random':
            trainData, testData, trainLabels, testLabels, trials = valClass.split_data_random(data)
        elif splitType == 'Trial':
            trainData, testData, trainLabels, testLabels, trials = valClass.split_data_trial(data)
        for trData, teData, trLabel, teLabel, trials in zip(trainData, testData, trainLabels, testLabels, trials):
            gridSearch.fit(trData, trLabel)
            bestParamList.append(gridSearch.best_params_)
            testPred = gridSearch.predict(teData)
            iterTrials = np.concatenate((trials, iterTrials), axis=None)
            iterTestLabels = np.concatenate((iterTestLabels, teLabel), axis=None)
            iterPredLabels = np.concatenate((iterPredLabels, testPred), axis=None)
            fold += 1
            print('Fold {} out of {} completed'.format(fold, valClass.NumFolds))

            if self._confusionSettings['confusionPerFold']:
                confMat = ConfusionMatrix(teLabel, testPred, classes=np.unique(trLabel))
                updateData.emit(confMat, 'conf_matrix', 'iter_%d_folds_%d' % (iteration, fold), True)

        return pd.DataFrame(index=iterTrials, columns=['trueLabel', 'predLabel'],
                            data=np.vstack((iterTestLabels, iterPredLabels)).transpose()), bestParamList






