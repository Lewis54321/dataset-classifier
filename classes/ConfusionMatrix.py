from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt
import numpy as np


class ConfusionMatrix:

    def __init__(self, trueLabel, predLabel, classes):

        self._trueLabel = trueLabel
        self._predLabel = predLabel
        self._classes = classes

    def as_matrix(self):
        """
        Method returns confusion matrix in matrix form
        :return cm:
        """

        cm = confusion_matrix(self._trueLabel, self._predLabel, labels=self._classes)
        return cm

    def as_fig(self, normalize=False, title=None, cmap=plt.cm.Blues):
        """
        Method for returning confusion matrix as a figure
        :param normalize:
        :param title:
        :param cmap:
        :return:
        """

        cm = confusion_matrix(self._trueLabel, self._predLabel, labels=self._classes)
        if not title:
            if normalize:
                title = 'Normalized confusion matrix'
            else:
                title = 'Confusion matrix, without normalization'

        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        fig, ax = plt.subplots()
        im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
        ax.figure.colorbar(im, ax=ax)
        # We want to show all ticks...
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=self._classes, yticklabels=self._classes,
               title=title,
               ylabel='True label',
               xlabel='Predicted label')

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center",
                        color="white" if cm[i, j] > thresh else "black")
        fig.tight_layout()
        return fig

    def generate_ax(self, fig, normalize=False, title=None, cmap=plt.cm.Blues):
        """
        Method for returning confusion matrix as a figure
        :param fig:
        :param normalize:
        :param title:
        :param cmap:
        :return:
        """

        cm = confusion_matrix(self._trueLabel, self._predLabel, labels=self._classes)
        if not title:
            if normalize:
                title = 'Normalized confusion matrix'
            else:
                title = 'Confusion matrix, without normalization'

        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        ax = fig.add_subplot(111)
        im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
        ax.figure.colorbar(im, ax=ax)
        # We want to show all ticks...
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               xticklabels=self._classes, yticklabels=self._classes,
               title=title,
               ylabel='True label',
               xlabel='Predicted label')

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center",
                        color="white" if cm[i, j] > thresh else "black")

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
