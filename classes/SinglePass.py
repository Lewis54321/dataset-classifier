from .PipelineDummyClass import DummyFeatureNorm, DummyResample, DummyFeatRed
from .ConfusionMatrix import ConfusionMatrix
from classes.neural_nets.cnn import cnn
from classes.neural_nets.rnn import rnn

import numpy as np
import pandas as pd

from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import mutual_info_classif
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import RFE
from sklearn.ensemble import ExtraTreesClassifier


class SinglePass:

    def __init__(self, dataSettings, featRedSettings, estSettings, valClass, confusionSettings, numIter):
        """
        Class implementation of a single pass classification (no hyper-parameter optimisation)
        :param dataSettings: Dict(string) [Data split and pre-processing settings]
        :param featRedSettings: Dict(string/ int) [Feature reduction settings]
        :param estSettings: Dict(string) [Estimator Settings]
        :param valClass: ValidationSplit Class Instance
        :param confusionSettings: Dict(bool) [Logic about what confusion matrices to output]
        :param numIter: int [Number of iterations to run the validation method]
        """

        # Data Settings
        self._splitType = dataSettings['splitType']
        resamp = {
            'None': DummyResample(),
            'Oversampling': RandomOverSampler(random_state=0),
            'Undersampling': RandomUnderSampler(random_state=0),
            'Synthetic Oversampling': SMOTE(random_state=0)
        }.get(dataSettings['resamp'])
        featNorm = {
            'None': DummyFeatureNorm(),
            'Standardisation': preprocessing.StandardScaler(),
            'Min-Max Normalisation': preprocessing.MinMaxScaler()
        }.get(dataSettings['featNorm'])

        # Feature Reduction Settings
        featRed = self.add_feat_red(featRedSettings['featRedType'], featRedSettings['featRedSubType'],
                                    featRedSettings['featRedBound'])

        # Estimator Settings
        estimator = self.add_classifier(estSettings['estName'])

        # Validation Class
        self._valClass = valClass

        # Confusion Matrix Settings
        self._confusionSettings = confusionSettings

        # Set up the pipe
        self._pipe = Pipeline([(dataSettings['featNorm'], featNorm), (dataSettings['resamp'], resamp),
                              (featRedSettings['featRedType'], featRed), (estSettings['estName'], estimator)])

        # Other parameters
        self._numIter = numIter

        # Summary
        dataSettings = ', '.join(key + ': ' + str(dataSettings[key]) for key in dataSettings)
        featParam = ', '.join(key + ': ' + str(featRedSettings[key]) for key in featRedSettings)
        confSettings = ', '.join(key + ': ' + str(confusionSettings[key]) for key in confusionSettings)
        if valClass.Name == 'ext_holdout':
            valSettings = 'Name: ext_holdout'
        else:
            valSettings = ', '.join(key + ': ' + str(valClass.__dict__[key]) for key in valClass.__dict__)
        self._summary = 'NUM ITER: ' + str(numIter) + '\nVAL SETTINGS: ' + valSettings + \
                        '\nDATA SETTINGS: ' + dataSettings + '\nFEAT SETTINGS: ' + featParam + \
                        '\nEST SETTINGS: ' + estSettings['estName'] + '\nCONFUSION SETTINGS: ' + confSettings

    @staticmethod
    def add_feat_red(featType, subType, thresh):
        """
        Method for adding feature reduction method depending on user parameters given
        :param featType: string [base feature reduction type]
        :param subType: string [sub feature reduction type]
        :param thresh: string/ int [feature reduction threshold]
        :return featRed:
        """

        featRed = None
        if featType is 'No_Reduction':
            featRed = DummyFeatRed()
        elif featType == 'Dimensionality Reduction':
            if subType == 'PCA':
                featRed = PCA(n_components=int(thresh))
            elif subType == 'LDA':
                featRed = LDA(n_components=int(thresh))
        elif featType == 'Univariate Reduction':
            if subType == 'chi-squared':
                featRed = SelectKBest(chi2, k=int(thresh))
            elif subType == 'ANOVA F-value':
                featRed = SelectKBest(f_classif, k=int(thresh))
            elif subType == 'mutual information':
                featRed = SelectKBest(mutual_info_classif, k=int(thresh))
        elif featType == 'Recursive Reduction':
            if subType == 'Linear SVC':
                featRed = RFE(estimator=SVC(kernel="linear"), n_features_to_select=int(thresh))
        elif featType == 'Importance Weights Reduction':
            if subType == 'Linear SVC':
                featRed = SelectFromModel(estimator=SVC(kernel="linear"), prefit=False, threshold=thresh)
            elif subType == 'Decision Tree':
                featRed = SelectFromModel(estimator=ExtraTreesClassifier(), prefit=False, threshold=thresh)
        return featRed

    @staticmethod
    def add_classifier(classText):
        """
        Method for adding an estimator implementation
        :param classText: string [estimator type to use]
        :return classifier:
        """

        classifier = None
        if classText == 'Logistic Regression':
            classifier = LogisticRegression(multi_class='auto', solver='lbfgs')
        elif classText == 'Linear Discriminant Analysis':
            classifier = LDA()
        elif classText == 'K Nearest Neighbours':
            classifier = KNN()
        elif classText == 'Decision Tree':
            classifier = DecisionTreeClassifier()
        elif classText == 'Support Vector Classifier':
            classifier = SVC(gamma='auto')
        elif classText == 'CNN':
            classifier = cnn(windowSize=200)
        elif classText == 'RNN':
            classifier = rnn(windowSize=200)

        return classifier

    def get_classify_thread(self, data, updateProg, updateMessage, updateData):
        """
        Method for performing a set number of iterations of cross-validation on a secondary thread
        :param data: pd.DataFrame [Index is PT/Window] [Columns are Features]
        :param updateProg: Signal
        :param updateMessage: Signal
        :param updateData: Signal
        :return: string
        """

        updateData.emit(self._summary, 'summary', '', False)
        labelDfList = list()
        for iteration in range(self._numIter):
            labelDf = self.class_data(data, self._pipe, self._valClass, iteration, self._splitType, updateData)
            labelDfList.append(labelDf)
            if self._confusionSettings['confusionPerIter']:
                confMat = ConfusionMatrix(labelDf['trueLabel'], labelDf['predLabel'], classes=np.unique(labelDf['trueLabel']))
                updateData.emit(confMat, 'conf_matrix', 'iter_%d_folds_all' % iteration, True)
            updateProg.emit('classify', iteration / float(self._numIter))

        labelDf = pd.concat(labelDfList, axis=0)
        if self._confusionSettings['confusionPerAnalysis']:
            confMat = ConfusionMatrix(labelDf['trueLabel'], labelDf['predLabel'], classes=np.unique(labelDf['trueLabel']))
            updateData.emit(confMat, 'conf_matrix_overall', 'iter_all_folds_all', True)
        classReport = classification_report(labelDf['trueLabel'], labelDf['predLabel'])
        updateData.emit(classReport, 'class_report', 'class_report', True)

        updateProg.emit('classify', 1)
        updateMessage.emit('classify', 'Completed Classification', 'statusbar')
        return 'classify'

    def class_data(self, data, pipe, valClass, iteration, splitType, updateData):
        """
        Implementation of random shuffle validation method
        :param data: pd.DataFrame [Index is PT/Window] [Columns are Features]
        :param pipe: imblearn pipeline instance
        :param valClass
        :param iteration: int
        :param splitType
        :param updateData
        :return labelDf
        """

        fold = 0
        iterTrials = np.array([])
        iterTestLabels = np.array([])
        iterPredLabels = np.array([])
        trainData, testData, trainLabels, testLabels, trials = [], [], [], [], []
        if splitType == 'Random':
            trainData, testData, trainLabels, testLabels, trials = valClass.split_data_random(data)
        elif splitType == 'Trial':
            trainData, testData, trainLabels, testLabels, trials = valClass.split_data_trial(data)
        for trData, teData, trLabel, teLabel, trials in zip(trainData, testData, trainLabels, testLabels, trials):
            pipe.fit(trData, trLabel)
            testPred = pipe.predict(teData)
            iterTrials = np.concatenate((trials, iterTrials), axis=None)
            iterTestLabels = np.concatenate((iterTestLabels, teLabel), axis=None)
            iterPredLabels = np.concatenate((iterPredLabels, testPred), axis=None)
            fold += 1
            print('Fold {} out of {} completed'.format(fold, valClass.NumFolds))

            if self._confusionSettings['confusionPerFold']:
                confMat = ConfusionMatrix(teLabel, testPred, classes=np.unique(trLabel))
                updateData.emit(confMat, 'conf_matrix', 'iter_%d_folds_%d' % (iteration, fold), True)

        return pd.DataFrame(index=iterTrials, columns=['trueLabel', 'predLabel'],
                            data=np.vstack((iterTestLabels, iterPredLabels)).transpose())






