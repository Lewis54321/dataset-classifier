from abc import ABC, abstractmethod


class DummyTransformer(ABC):

    @abstractmethod
    def fit(self):
        pass

    @abstractmethod
    def transform(self):
        pass


class DummyFeatureNorm(DummyTransformer):

    def fit(self, data, y=None):

        return self

    def transform(self, data):

        return data


class DummyFeatRed:

    def __init__(self):

        self._xData = None
        self._yData = None

    def fit(self, xData, yData):

        self._xData = xData
        self._yData = yData
        return self

    def transform(self, data):

        return data


class DummySampler(ABC):

    @abstractmethod
    def fit_resample(self, x, y):
        pass


class DummyResample(DummySampler):

    def __init__(self):
        pass

    def fit_resample(self, x, y):
        return x, y







