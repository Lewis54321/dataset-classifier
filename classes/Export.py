import numpy as np
import os


class Export:

    def __init__(self, exportDir):
        """
        Class dedicated to exported classification data
        :param exportDir: str [Directory to be created and exported to]
        """

        self._exportDir = exportDir
        os.mkdir(exportDir)

    def export_fig(self, fig, heading):
        """
        Method dedicated to exporting of figures
        :param fig: matplotlib.figure
        :param heading: string
        """

        fig.savefig(fname=os.path.join(self._exportDir, heading))

    def export_text(self, textString, heading):
        """
        Method dedicated to exporting classification report
        :param textString: string
        :param heading: string
        """

        fileName = os.path.join(self._exportDir, heading)
        with open(fileName + '.txt', 'w') as f:
            f.write(textString + '\n')

