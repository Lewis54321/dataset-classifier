from sklearn.base import BaseEstimator
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import RFE


class BaseFeatSelect(BaseEstimator):

    def __init__(self, featSelect, numFeat=None):
        """
        A Custom BaseFeatSelect that can switch between feature selection classes.
        :param featSelect: SKLearn Estimator
        """

        self.featSelect = featSelect
        self.numFeat = numFeat

    def fit(self, X, y=None, **kwargs):
        self.featSelect.fit(X, y)
        return self

    def transform(self, data):
        transData = self.featSelect.transform(data)
        return transData

    def fit_transform(self, X, y=None, **kwargs):
        if isinstance(self.featSelect, PCA) or isinstance(self.featSelect, LDA):
            self.featSelect.n_components = self.numFeat
        elif isinstance(self.featSelect, SelectKBest):
            self.featSelect.k = self.numFeat
        elif isinstance(self.featSelect, RFE):
            self.featSelect.n_features_to_select = self.numFeat
        U = self.featSelect.fit_transform(X, y)
        return U


class DummyFeatSelect(BaseEstimator):

    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, data):
        return data

    def fit_transform(self, X, y=None, **kwargs):
        return X



