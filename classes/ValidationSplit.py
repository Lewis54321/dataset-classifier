from sklearn.model_selection import train_test_split
from abc import abstractmethod, ABC
from sklearn.model_selection import KFold, StratifiedKFold
import numpy as np


class ValidationSplit(ABC):

    def __init__(self, name, numFolds):
        """
        Base class for all validation classes
        :param numFolds
        :param name
        """
        self.Name = name
        self.NumFolds = numFolds

    @abstractmethod
    def split_data_random(self, data):
        pass

    @abstractmethod
    def split_data_trial(self, data):
        pass


class ValidationExtHoldOut(ValidationSplit):

    def __init__(self, extTestSet):

        super().__init__(name='ext_holdout', numFolds=1)
        self._extTestSet = extTestSet

    def split_data_random(self, trainData):
        """
        Method for extracting split data from train and test data
        :param trainData: pd.DataFrame
        :return:
        """

        # Sort columns to ensure features are ordered same for training and test set
        trainData.sort_index(axis=1, inplace=True)
        self._extTestSet.sort_index(axis=1, inplace=True)
        trainLabels = trainData.index.get_level_values('label').values
        testLabels = self._extTestSet.index.get_level_values('label').values
        trials = np.repeat(1, len(testLabels))

        return [trainData.values], [self._extTestSet.values], [trainLabels], [testLabels], [trials]

    def split_data_trial(self, trainData):
        """
        Method for extracting split data from train and test data
        :param trainData: pd.DataFrame
        :return:
        """

        # Sort columns to ensure features are ordered same for training and test set
        trainData.sort_index(axis=1, inplace=True)
        self._extTestSet.sort_index(axis=1, inplace=True)
        trainLabels = trainData.index.get_level_values('label').values
        testLabels = self._extTestSet.index.get_level_values('label').values

        return [trainData.values], [self._extTestSet.values], [trainLabels], [testLabels]


class ValidationHoldOut(ValidationSplit):

    def __init__(self, internalTestSize):
        """
        Validation hold out implementation
        :param internalTestSize: Int [Percentage proportion of data to be used as the test set]
        """

        super().__init__(name='int_holdout', numFolds=1)
        self._intTestSize = internalTestSize / float(100)

    def split_data_random(self, data):
        """
        Method for randomly splitting randomly
        :param data: pd.DataFrame
        :return:
        """

        trainData, testData = train_test_split(data, test_size=self._intTestSize, random_state=1)
        trainLabels = trainData.index.get_level_values('label').values
        testLabels = testData.index.get_level_values('label').values
        trials = testData.index.get_level_values('trial').values

        return [trainData.values], [testData.values], [trainLabels], [testLabels], [trials]

    def split_data_trial(self, data):
        """
        Method for splitting data by trial
        :param data: pd.DataFrame
        :return:
        """

        trialUnique = data.index.unique(level='trial').to_numpy()
        # Swapping the outer index to be trial (rather than label) for easy indexing of trials
        data = data.swaplevel(i='label', j='trial')

        trainTrials, testTrials = train_test_split(trialUnique, test_size=self._intTestSize, random_state=1)
        trainData = data.loc[trainTrials]
        testData = data.loc[testTrials]
        trainLabels = trainData.index.get_level_values('label').values
        testLabels = testData.index.get_level_values('label').values

        return [trainData.values], [testData.values], [trainLabels], [testLabels], [testTrials]


class ValidationCross(ValidationSplit):

    def __init__(self, stratify, numFolds):
        """
        Cross-Validation Implementation
        :param stratify: Bool [Whether or not to stratify validation folds]
        :param numFolds: Int [Number of folds in cross-validation]
        """

        super().__init__(name='crossval', numFolds=numFolds)
        self._stratify = stratify

    def split_data_random(self, data):
        """
        Method for splitting data randomly
        :param data: pd.DataFrame
        :return:
        """

        trainData, testData = list(), list()
        trainLabels, testLabels = list(), list()
        allLabels = data.index.get_level_values('label').values
        trials = list()
        if self._stratify:
            kFold = StratifiedKFold(n_splits=self.NumFolds, shuffle=True)
        else:
            kFold = KFold(n_splits=self.NumFolds, shuffle=True)

        for train_idx, test_idx in kFold.split(data, allLabels):

            trainData.append(data.iloc[train_idx].values)
            testData.append(data.iloc[test_idx].values)
            trainLabels.append(data.iloc[train_idx].index.get_level_values('label').values)
            testLabels.append(data.iloc[test_idx].index.get_level_values('label').values)
            trials.append(data.iloc[test_idx].index.get_level_values('trial').values)

        return trainData, testData, trainLabels, testLabels, trials

    def split_data_trial(self, data):
        """
        Method for splitting data by trial
        :param data: pd.DataFrame
        :return:
        """

        trainData, testData = list(), list()
        trainLabels, testLabels = list(), list()
        trials = list()

        kFold = KFold(n_splits=self.NumFolds, shuffle=True)
        uniqueTrials = data.index.unique(level='trial').values
        # Swapping the outer index to be patient (rather than label) for easy indexing of patients
        data = data.swaplevel(i='label', j='trial')

        for train_idx, test_idx in kFold.split(uniqueTrials):

            trainData.append(data.loc[uniqueTrials[train_idx]].values)
            trainLabels.append(data.loc[uniqueTrials[train_idx]].index.get_level_values(level='label').values)
            testData.append(data.loc[uniqueTrials[test_idx]])
            testLabels.append(data.loc[uniqueTrials[test_idx]].index.get_level_values(level='label').values)
            trials.append(data.loc[uniqueTrials[test_idx]].index.get_level_values(level='trials').values)

        return trainData, testData, trainLabels, testLabels, trials
