from tensorflow import keras
from .nn_imblearn_pipeline import nn_imblearn_pipeline


class cnn(nn_imblearn_pipeline):

    def __init__(self, windowSize, filter1=64, filter2=64, lossFunction='categorical_crossentropy'):

        self._windowSize = windowSize
        self.filter1 = filter1
        self.filter2 = filter2
        if windowSize == 1:
            self._kernelSize = 1
            self._poolSize = 1
        else:
            self._kernelSize = 3
            self._poolSize = 2

        self._epochs = 10
        self._batchSize = 32

        super(cnn, self).__init__(windowSize, self._epochs, self._batchSize, lossFunction)

    def gen_model(self, numFeat, numClasses):
        """
        Method for generating the CNN model
        :param numFeat:
        :param numClasses:
        :return:
        """

        super(nn_imblearn_pipeline, self).__init__(name='cnn')

        self._conv1 = keras.layers.Conv1D(filters=self.filter1, kernel_size=self._kernelSize, activation='relu',
                                          input_shape=(self._windowSize, numFeat))
        self._conv2 = keras.layers.Conv1D(filters=self.filter2, kernel_size=self._kernelSize, activation='relu')
        self._dropout = keras.layers.Dropout(0.5)
        self._maxpool = keras.layers.MaxPooling1D(pool_size=self._poolSize)
        self._flatten = keras.layers.Flatten()
        self._dense1 = keras.layers.Dense(100, activation='relu')
        self._dense2 = keras.layers.Dense(numClasses, activation='softmax')

    def call(self, inputs):

        x1 = self._conv1(inputs)
        x2 = self._conv2(x1)
        x3 = self._dropout(x2)
        x4 = self._maxpool(x3)
        x5 = self._flatten(x4)
        x6 = self._dense1(x5)
        return self._dense2(x6)
