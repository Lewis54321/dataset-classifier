from tensorflow import keras
from .nn_imblearn_pipeline import nn_imblearn_pipeline


class rnn(nn_imblearn_pipeline):

    def __init__(self, windowSize, lossFunction='categorical_crossentropy'):

        self._windowSize = windowSize

        self._epochs = 10
        self._batchSize = 32

        super(rnn, self).__init__(windowSize, self._epochs, self._batchSize, lossFunction)

    def gen_model(self, numFeat, numClasses):

        super(nn_imblearn_pipeline, self).__init__(name='rnn')

        self._lstm = keras.layers.LSTM(100, input_shape=(self._windowSize, numFeat))
        self._dropout = keras.layers.Dropout(0.5)
        self._dense1 = keras.layers.Dense(100, activation='relu')
        self._dense2 = keras.layers.Dense(numClasses, activation='softmax')

    def call(self, inputs):

        x1 = self._lstm(inputs)
        x2 = self._dropout(x1)
        x3 = self._dense1(x2)
        return self._dense2(x3)
