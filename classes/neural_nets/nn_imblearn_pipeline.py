import numpy as np
from tensorflow import keras
from sklearn.base import BaseEstimator
from abc import abstractmethod
from sklearn.preprocessing import LabelBinarizer


class nn_imblearn_pipeline(keras.Model, BaseEstimator):
    """
    Base class for achieving compatibility of tensorflow implementation of neural nets within the imblearn pipeline
    Exposes methods fit, score, and predict which is required of estimators in the pipeline
    """

    def __init__(self, windowSize, epochs, batchSize, lossFunction):
        """
        Initializer
        :param windowSize:
        :param epochs:
        :param batchSize:
        :param lossFunction:
        """

        self._windowSize = windowSize
        self._epochs = epochs
        self._batchSize = batchSize
        self._lossFunction = lossFunction
        self._encoder = LabelBinarizer()

    @abstractmethod
    def gen_model(self, numFeat, numClasses):
        pass

    @abstractmethod
    def call(self, inputs):
        pass

    def fit(self, trainData, trainLabels, optimiser='adam'):
        """
        Method for fitting the model to the training data
        :param trainData: array-like
        :param trainLabels: array-like
        :param optimiser:
        :return:
        """

        numFeat = trainData.shape[1]
        numClasses = np.unique(trainLabels)

        self.gen_model(numFeat, numClasses)
        self.compile(loss=self._lossFunction, optimizer=optimiser, metrics=['accuracy'])
        trainData = trainData.reshape((int(trainData.shape[0] / float(self._windowSize)), self._windowSize, trainData.shape[1]))
        trainLabels = trainLabels[0::self._windowSize]
        self._encoder.fit(trainLabels)
        encodedTrainLabels = self._encoder.transform(trainLabels)
        super(nn_imblearn_pipeline, self).fit(trainData, encodedTrainLabels, epochs=self._epochs, batch_size=self._batchSize)

    def score(self, testData, testLabels):
        """
        Method for assigning accuracy score
        :param testData:
        :param testLabels:
        :return:
        """

        testData = testData.reshape((int(testData.shape[0] / float(self._windowSize)), self._windowSize, testData.shape[1]))
        testLabels = testLabels[0::self._windowSize]
        encodedTestLabels = self._encoder.transform(testLabels)
        _, accuracy = self.evaluate(testData, encodedTestLabels, batch_size=self._batchSize)
        return accuracy

    def predict(self, testData):
        """
        Method for predicting unseen data
        :param testData:
        :return:
        """

        testData = testData.reshape((int(testData.shape[0] / float(self._windowSize)), self._windowSize, testData.shape[1]))
        encodedPredictions = super().predict(testData)
        predictions = self._encoder.inverse_transform(encodedPredictions)
        return predictions
