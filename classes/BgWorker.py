from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
import traceback


class WorkerClass(QObject):

    # Internally managed signals
    finished = pyqtSignal(str)
    error = pyqtSignal(BaseException)

    # Externally managed signals
    updateProg = pyqtSignal(str, float)
    updateMessage = pyqtSignal(str, str, str)
    updateData = pyqtSignal(object, str, str, bool)

    def __init__(self, fn, *args, **nargs):

        super(self.__class__, self).__init__()
        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.nargs = nargs

    @pyqtSlot()
    def run_function(self):

        try:
            stage = self.fn(*self.args, **self.nargs, updateProg=self.updateProg, updateMessage=self.updateMessage,
                            updateData=self.updateData)
            self.finished.emit(stage)

        except BaseException as exception:
            if exception.args[0] == 'noncritical':
                self.finished.emit('noncritical_except')
                return
            traceback.print_exc()
            self.error.emit(exception)




